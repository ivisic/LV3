import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

br=0
mtcylsv6=0
mtcylsv4=0
amcars=0
amcar100=0
mtcars = pd.read_csv('mtcars.csv')  

mtmax=mtcars.sort(['mpg'], ascending=[0])  
print("5 automobila s najvecom potrosnjom: \n", mtmax[:5])

mtcylmin=mtcars[mtcars.cyl==8].sort(['mpg'], ascending=[1]) 
print("  3 automobila s 8 cilindara koji imaju najmanju potrosnju: \n", mtcylmin[:3])

mtcylsv6+=mtcars[mtcars.cyl==6].mpg 
print(  "Srednja potrošnja automobila sa 6 cilindara : ", sum(mtcylsv6)/len(mtcylsv6)) 

mtcylsv4+=mtcars[(mtcars.cyl==4) & (mtcars.wt>=2) & (mtcars.wt<=2.2)].mpg 
print( " Srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs : ", sum(mtcylsv4)/len(mtcylsv4))

amcars+=mtcars[mtcars.am==0].mpg 
print(" U skupu je", len(amcars), "automobila sa automatskih mjenjačem, a", len(mtcars)-len(amcars), "s ručnim mjenjačem.")

amcar100+=mtcars[(mtcars.am==0) & (mtcars.hp>=100)].mpg 
print(" U skupu je", len(amcar100), "s automatskim mjenjačem i snagom preko 100 konjskih snaga.")

mtcars['kg'] = mtcars.wt*1000*0.45359237 
print(" Masa pojedinog automobila u kilogramima:\n", mtcars[['car','kg']])


